---
keywords:
    - SVD
    - HOSVD
    - RPCA
    - tensorly
    - python
title: 'SVD and HOSVD'
date: 02.11.2017
---

$\newcommand{\tens}{\mathcal}\newcommand{\R}{\mathbb{R}}\newcommand{\trans}[1]{#1^{\top}}$

Today I tried different option using the **SVD** and the **HOSVD** to increase the sparsity of an image and to improve the reconstruction of the signal.

## SVD

In the first experiment I loaded the YaleB data-set (of size l x m x n) and stored in a vectorialized (l form on one side and *fft2* transformed on the other side.

In such way I compared the performance of the f_avg and the adaptive sub-sampling in both FFT and SVD domain. The SVD outperformed the FFT2 in both cases, by long.

The decomposition is done as following, given $X \in \R^{l\times o}$ the training data-set wiyh $L$ vectorialized images $\vec{x}_i\in \R^{o=m\cdot n}$:
$$
X_{l\times o} = U_{l\times l}\Sigma_{l\times o} V^*_{o\times o}
$$



Where $U$ and $V$ are ortho-normal bases. In particular the base $V \in \R^{o\times o}$ will be the one used to sparsify the future signals $\vec{y}_i$. 

Given a new image $\vec{y}_i\in \R^{o}$ the compression of factor $n / o$ and reconstruction will be computed as:
$$
\vec{y}_{svd} = V_{o\times o}\vec{y}_i\\
\vec{y}_{svd}[n:o] = 0\\
\hat{y_i} = V_{o\times o}^{T}\vec{y}_{svd}
$$
Implemented in python as follow:

```python
U, s, V = np.linalg.svd(X) 	# decomposition in UxSxV.T
y_svd = V.dot(y) 			# projection on base V
y_svd[n:] = 0 				# compression 
y_rec = V.T.dot(y_svd)		# reconstruction
```

For the *YaleB* dataset resulted in the following performances with different components selection method compared to the *FFT2* transform::

![plot](svd_plot.png)

## HOSVD

The **HOSVD** is the tensor equivalent of the **SVD** matrix decomposition. Given a tensor $\tens{A}\in \R^{I_1\times I_2\times\dots\times I_N}$ the SVD decomposition of its $k$-mode folding $\tens{A}_{(k)}$ is:
$$
\tens{A}_{(k)}=U_{k}\Sigma_KV^T_k
$$
however for each of the $N$ modes of the tensor we are interested only in the $U_k$ matrix (and otho-normal base). Finally the core tensor $\tens{S}\in \R^{I_1\times I_2\times\dots\times I_N}$ (the equivalent of $\Sigma$ in the classical SVD) is computed as follow:
$$
\tens{S}=\tens{A}\times^N_{n=1}U^T_n
$$
in Python I used the [TensorLy](https://tensorly.github.io/) library to decompose the tensor using the tucker algorithm:

```python
import tensorly as tl
from tensorly.decomposition import tucker

A = load_dataset(path)
S, U = tucker(A, n_iter_max=50)
```

 

### Uses

A first use of this decomposition is tu use the orthonormal basis to sparsify the new signal (a sort of 2D SVD), the mean magnitude of the sparsified signal is shown in the following picture.

![Average magnitude for training dataset](avg_spect.png)

The transformation was done as follow:
$$
Y_{hosvd}= U_3^T(U_2^TY)^T\\
\hat{Y}=U_2(U_3Y_{hosvd})^T
$$
where the compression can be done as usual by zeroing the lower rank components. However even if the resulting reconstruction was better than using the *FFT2* the performances were inferior to using the classical SVD.

![Results](hosvd_plot.png)

Another possible use is to denoise an image by stacking the patches of the image itself and then performing the *HOSVD* on the constructed tensor. However today I was not able to reconstruct the tensor once decomposed. I will try more tomorrow.

I want as well investigate alternative way to use the HOSVD as base.

##RPCA


Moreover I reproduced the results of the **Robust PCA**  using the TensorLy library as shown in the blog post [RPCA](https://jeankossaifi.github.io/tensorly/rpca.html) from Jean Koassaifi.

It seems that the reasons why I did not get any good results from the first test was because of the normalization ([0..1]) of the loaded images. Probably  this can be fixed by tuning the parameters of the *r_pca* (maybe the *reg_E* or the *learning_rate*).

![RPCA denoising](rpca_res.png)

