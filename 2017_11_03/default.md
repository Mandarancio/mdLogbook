---
keywords:
    - HOSVD
    - algorithm
    - python
    - dataset
title: 'HOSVD Analysis'
date: 03.11.2017
---

$\newcommand{\tens}{\mathcal}\newcommand{\R}{\mathbb{R}}\newcommand{\trans}[1]{#1^{\top}}$

## Introduction to the HOSVD decomposition

### Tucker Method

As  already explained yesterday the HOSVD Tucker decomposition of a tensor $\tens{A}\in \R^{I_1\times I_2\times\dots\times I_N}$ in a core tensor $\mathcal{S}$ of the same dimension of $\tens{A}$ and $N$ orthonormal matrices $U_i \in \R^{I_i\times I_i}$. The computation can be done in two step:
$$
\tens{A}_{(i)}=U_i\Sigma_i\trans{V}_k
$$

$$
\tens{S}=\tens{A}\times^N_{i=1}\trans{U}_i
$$

where $\tens{A}_{(i)}$ s the folded tensor in the $i^{th}$ mode.

The recomposition in the iterative mode can be computed as following:
$$
a_{i_1,i_2,\dots,i_N}=\sum_{j_1}\sum_{j_2}\cdots\sum_{j_N}s_{j_1,j_2,\dots,j_n}u^{(1)}_{i_1,j_1}u^{(2)}_{i_2,j_2}\dots u^{(n)}_{i_n,j_n}
$$

#### Python Implementation

```python
import numpy as np
import tensorly as tl
import tools


def __compute_component__(S, Us, i1, i2, i3):
    """Compute A[i1, i2, i3]."""
    comp = 0
    shape = S.shape
    for j1 in range(shape[0]):
        for j2 in range(shape[1]):
        	for j3 in range(shape[2]):
				comp += S[j1, j2, j3]*\
                		Us[0][i1, j1]*Us[1][i2, j2]*Us[2][i3, j3]
    return comp


def recompose(S, Us):
    """Recompose tensor from 3D SVD decomposition."""
    A = np.zeros(S.shape, dtype= S.dtype)
    shape = A.shape
    for i1 in range(shape[0]):
        for i2 in range(shape[1]):
            for i3 in range(shape[2]):
                A[i1, i2, i3] = __compute_component__(S, Us, i1, i2, i3) 
	return A


# use numpy backend to semplify the code.
tl.use_backend('numpy') 

# load dataset as tensor (by default)
X = tools.load_dataset('YaleB-training/')

# tucker HOSVD using tensorly (it can take time)
S, Us = tl.decomposition.tucker(X)
# compression
S[20:, 20:, 20:] = 0
# reconstruction
Xr = recompose(S, Us)
```

However this implementation is very slow and as is of order $\tens{O}((I_1I_2I_3I)^2)$ but less memory consuming.

## Tensor Rank Decomposition

Another possible decomposition for a tensor $\tens{A}$ is the Tensor Rank Decomposition or **CPD**. In this case the tensor can be represented as:
$$
\tens{A}=\sum_{i=1}^r\lambda_ia_i^1\otimes a_i^2\otimes\cdots\otimes a_i^d
$$
where $r$ is the rank of the tensor, $\lambda_i \in \R$ is a weight and $a^j_i \in \R^{n_j}$ is a vector of size $n_j$.

The decomposition using *Tensorly* is again very simple and is enough to call the method ```tl.decomposition.parafac(X)``` to retrieve the $r$ factors.

### Results

The results on the *YaleB* dataset are very good as shown in the notebook [HOSVD Images](http://127.0.0.1:8888/notebooks/TensorMath/HOSVD_Images.ipynb), and in the following figure and result table.

![Reconstruction](HOSVD_compression.png)

| Method         |         Compression Rate |          mse |
| -------------- | -----------------------: | -----------: |
| FFT2 $f_{avg}$ |     136/2900 ($4.7e^-2$) | $1.11e^{-2}$ |
| HOSVD *CPD*    | 40800/870000 ($4.7e^-2$) | $4.90e^{-3}$ |
| HOSVD *Tucker* | 96864/870000 ($1.1e^-1$) | $4.34e^{-2}$ |

The **CPD** seems to perform very well and as well seems to be **much** faster then the **Tucker**. However its possible than this is due to the reconstruction algorithm implemented or by the uniform selection of indexes over the three dimensions of the dataset (even if the 3th dimension was around 6 times bigger than the two).

Another possible test to do is to see the differences between the classical **CPD** and the **Non negative CPD**.



## IRIS Dataset

The *IRIS* dataset contains pictures and spectra from the *IRIS LMSAL mission*, targeted to study solar eruption. The data can be visualized and downloaded at the [IRIS data search](http://iris.lmsal.com/search/) portal.

A *Jupyter* notebook with few tests can be found [here](http://127.0.0.1:8888/notebooks/TensorMath/IRIS%20Loader.ipynb).

Each *fit* file contains a sequence of images as well as the translation of the sun from the probe. To read this format I made use of the library *astropy* that make the loading and use of the files very simple.

![iris_lr_compress](iris_lr_compress.png)

The sequence can be both stationary or moving around the sun, in the previous figure is possible to see a frame from a stationary image in the compressed and non-compressed low resolution format.

The compression using the *CPD* decomposition over a sequence is very effective, as well I am interested at using the *RPCA* for denoising and reconstruction purpose in particular for non-fixed sequences where the missing data can be reconstructed from the other frames.



## CPD Factors as Bases

Finally I tried to use the *CPD* factors computed from the training dataset as bases for the future samples. First I remember that the *CPD* decomposition of a tensor $\tens{A}$ results in $N$ matrix $A_i\in \R^{I_i\times r}$ where $N$ is the number of dimension of the tensor, and $I_i$ is the size of the $i^{th}$ dimension and $r$ is the requested rank. 

In our case the matrices will be:
$$
A_0^{L\times r}, A_1^{W\times r}, A_2^{H\times r}
$$
where $L$ is the number of images on the training set, $W$ the width of the images and $H$ the height of the images. The first factor $A_0$ in our case is useless as we want to compress and decompress a new image from a different set and not reconstruct our original dataset, and we will use only $A_1$ and $A_2$ for this purpose.

Given $X\in\R^{W\times H}$ the image to compress we will proceed as following:
$$
C_x\in\R^{r\times W} = \trans{A_1}\cdot X\\
C_{x,y}\in\R^{r\times r}=\trans{A_2}\cdot\trans{C_x}
$$

The compression depends on the chosen rank $r$ of the dataset decomposition and it looks something like the image below.

![kernel](cpd_decomposition.png)

To recover the signal from this representation its need to use the pseudo inverse matrices of $A_1$ and $A_2$:
$$
\hat{X}\in\R^{W\times H}=A_1^+\trans{(A_2^+C_{x,y})}
$$
The reconstruction is very fast and effective and give great results as shown in the picture and table below.

![Results for r=30](cpd_compression.png)

| Method            | Compression Rate |          MSE |
| ----------------- | ---------------: | -----------: |
| FFT2 **Adaptive** |  900/2900 (0.31) | $6.25e^{-3}$ |
| CPD projection    |  900/2900 (0.31) | $9.00e^{-4}$ |

