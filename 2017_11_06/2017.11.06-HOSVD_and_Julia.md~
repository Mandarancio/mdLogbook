# 06.11.2017 - HOSVD and Julia

## HOSVD CPD

I continue the experimentation with the **tensor rank decomposition** (also know as **canonical polyadic decomposition (CPD)**). I want to understand how the noisy images can be restored using the learned factors and how I can improve the compression.

###CPD and noisy images

The first thing I tried is to use different rank $r$ to decompose my training set $\tens{X}$ and to see how the reconstruction vary in function of $r$. As is possible to see in the following figures the CPD Basis works pretty well:

![Original](imgs/2017.11.06/original.png)![Noisy](imgs/2017.11.06/noisy.png)![Recover r=30](imgs/2017.11.06/noise_rec_r30.png)

The first picture is the original sample, the second one is the noisy sample and finally the third one is the recovered using a rank $r=30$.

### CPD Sparsification

I wanted to quantify the level of sparsification that can be achieved with the CPD factors as bases.

The histogram of the transformed signal has the shape shown in the next figure, whit small variation depending on the rank chosen.

![Histogram](imgs/2017.11.06/sparsification.png)

It could be then possible to shrink even more the data around zero to get a more sparse and compress data.

## Julia

I wanted to give a try to this *new* language specifically designed for maths and data analysis and that should have high performance (on the *C* order) and easy learning curve and fast development (on the *Python* order).

