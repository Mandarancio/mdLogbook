---
title: 'Julia and more CPD'
date: 07.11.2017
---

## Julia

Julia is an high performance math and data-analysis language. Using *Atom* editor and its extension *Juno* or the *IJulia* kernel for *Jupyter* it is possible to have a very nice IDE.

The loading, plotting and transformation of the images is quite simple and fast, I put the code in a small library called *Utilis.jl*. However using the last git version of *TensorDecompositions* to perform the CPD and other decomposition of the tensors doesn't seem to be faster than the python (*TensorLy*)  implementation. And due to the compilation overhead for every execution I preferred to continue the prototyping on Python.

## More and More CPD

Today tests are more focused on the performances of the compression over different ranges of *ranks* and as well how it works in already transformed data. 

To do so I choose to use the *DCT* domain as its results in a real tensor (complex number are not yet supported by the *CPD* implementation). I will compare the CPD compression in the direct domain against the CPD in the *DCT* domain and the *adaptive DCT* compression.

![Difference in performance](mse_results.png)

As possible to see from the plot in the previous figure the *CPD* Compression in the direct domain perform very well, while one the *DCT* domain it has a very similar trend to the one of the standard *DCT* adaptive compression (a.k.a *Best-K*). 

Is possible to see this trend as well in the reconstructed images (here with a compression rate of 25/32256).

![Reconstruction with 25 samples](rec_results_k25.png)

