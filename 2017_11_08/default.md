---
title: 'Julia performances and Haskell'
date: 08.11.2017
---

## Julia performance

After first non numerical approach to measure the performance of Julia, I tried a more systematic one, using the *time* function I measured the time needed to perform the **nnpc** for both python *TensorLy* implementation and Julia *TensorDecompositions* implementation. It results in a small performance gain for rank $r=50$ and with a a dataset of size $500\times168\times192$ the python version need around 90 seconds while the julia takes only 70 seconds in average. 

However the implementation is different, while the python use a initial value extrapolated from the *SVD* of the folded tensor the Julia use a random initialization. As well the python as a convergence factor of $1e^{-6}$ while Julia only of $1e^{-4}$ (configurable in both cases).

However the real problem is the quality of the reconstruction, due probably both for the different initialization property and for the implementation differences. The Julia reconstructed images present many artifacts and the overall *mse* is worst (and diverge) from the *adaptive dct* compression.

## Haskell

Haskell is a beautiful functional and lazy language, it has moreover a great variety of libraries (such as: *hTensor, HIP, hFFTW, etc etc*). 

However due to a change in the *Archlinux* package of stack and other library I had to reinstall and recompile most of the libraries and tools.

### IHaskell

I'm interested in try to install and use *IHaskell* as ide for the language.

