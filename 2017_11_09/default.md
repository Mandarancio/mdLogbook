---
date: 09.11.2017
title: 'Generative Model and Overcomplete DCT'
---

## Generative Model

The idea of a generative model is to be able to generate a signal from some small initial oscillation. This initialization signal can be both random or a sub sampled real signal.

We are particularly interested to the second case, using a  training set and a filter set it will be possible to first make clusters of the filtered dataset and then training a classifier for choosing the centroid for each filter. At this point it should be possible to remodel the centroid using the subsample signal to have a closest representation of the original signal.

## Overcomplete DCT

The over complete *DCT* is obtained by convolving the dictionary of the *DCT* kernel at different spatial frequencies with the images and keeping the whole samples.

The *DCT* $8\times8$ dictionary is the following:
![DCT dictionary](Dctjpeg.png)

However there is no reasons to use all the 64 patterns but a small subset is enough, in particular I used 9 for transforming a sample image from the *YaleB* dataset.

 ![Over complete transformation](example.png)

The filtering is like pass band filters in from low frequency to higher in both vertical diagonal and horizontal directions.

