---
date: 10.11.2017
title: 'Generation and Reconstruction'
---

## FFT DCT Overcomplete

I preferred to perform the overcomplete DCT transformation  in the frequency domain, instead of convolving the filter to the image I first pad the filter with zeros to fit the image size, then I transform it using the *FFT* transform and multiply it with the spectrum of the image, eventually using a normalization factor $\epsilon$.

The reconstruction can be done using the *pseudo inverse* of the *FFT* of the filters or **in the future** by converting it in the frequency domain again. 

For example given this over complete transformation:

![](example.png)

It is possible to quickly reconstruct:

![](reconstruction.png)



## Generation

Once transformed all the training set and clustered it, it is possible to generate new images from this by simply choosing random centroids for different frequency, for the *YaleB* dataset it results in something like this:

![random_generation](random_generation.png)

It is possible to see that many artifacts are present in the image, but however the generation is very fast and relatively look a like. 

By using the cluster generation information is possible to give more bounders to the generation and control better this process this results in better (but less *variate*) generations:

![bondaries_gen](bondaries_gen.png)

![semi_random_generation](semi_random_generation.png)

I want to try this process with the huge *CELEBa* dataset to see what it can be the results.



## Sampling and reconstruction

To be able to use this technique to enhance the reconstruction of the signal we need to first to choose the sampling mask for each filter level that maximize the distinction between the clusters. 

For the moment I'm simply using the spot that maximize the variance for each layer, the classifier is as well very simple and simple minimize the *mse* between the cluster centroid and the sampled signal.

Than  there is the selection policy, the simplest policy is to don't put any boundaries to the selection of the clusters for ever layer. However this give mixed results, a simple possible solution is as in the generative process is to use the relationship between the images that generated the selected cluster of the previous layer to restrict the number of possible centroids of the current layer as shown in the next figure:



![naive_option](naive_option.png)

In this configuration it will be possible to using a variable number of samples for each layer and as well accumulate to the samples to get preciser reconstruction.

Another possibility is to explore the whole tree of linked centroids and then select the best configuration a *posteriori*, maybe after explored the reconstruction. However I did not tried to implement it **yet**.

![dynamic_option](dynamic_option.png)

 The initial results are mixing probably due to the fact that the dataset is not well aligned and as well the algorithm is very naive.

![sampling](sampling.png)

With the *OASIS* dataset it perform slightly better:

![mse_rec](oasis_rec.png)

![oasis_mse](oasis_mse.png)



However the real compression rate of the *Generative* model is actually much smaller than the *Best-K* as many samples between layers are shared, as well the classifier is very naive.



### CELEBa generation

An example of generation based on the *CELEBa* dataset with a semi-naive generation and based on only 128 cluster per level give the following results:

![celeba_128k](celeba_128k.png)

