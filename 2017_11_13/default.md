---
date: 13.11.2017
title: 'More on Generative Models'
---

$\newcommand{\tens}{\mathcal}\newcommand{\R}{\mathbb{R}}\newcommand{\trans}[1]{#1^{\top}}$

## Again reconstruction

Last week the reconstruction from the sub-bands over complete *DCT* was done in such simple way:
$$
\hat{x}=\frac{1}{L}\sum_{i=1}^{L}\tens{F}^{-1}(\tens{X}_{b_i}\cdot (\tens{H}_i+\Delta)^{-1})
$$

with $L$ the total number $\tens{F}$ the *Fourier* transform, $\tens{X}_{b_i}$ the spectrum of the $i^{th}$ sub band, $\tens{H}_i$ the $i^{th}$ band filter and $\Delta$ a small regularization factor. Those means that for each sub-band it try to reconstruct the full spectrum of the signal and then sum up the $L$ images.

While for the reconstruction of one signal decomposition it could work perfectly it is probably not the best approach for the generative case as it is comparable to sum up the $L$ whole centroids selected in the direct  domain and to lose the link between sub-bands and spatial reconstruction.

An alternative implementation of the reconstruction is the following
$$
\hat{x} = \tens{F}^{-1}\left(\sum_{i=1}^L\left(\tens{X}_{b_i}\cdot(\tens{H_i+\Delta})^{-1}\cdot|\tens{H_i}|)\right)\cdot\sum_{i=1}^L|\tens{H}_i|^{-1}\right)
$$
the formula is very similar however instead of performing the normalization on the direct domain with uniform weight I do it in the *Fourier* domain with a variable weight that is nothing else then the magnitude of the sub band filter $|\tens{H}_i|$.

In this way each sub band should contribute most on the correct part the spectrum .

Even without further normalizations this give better results than the first reconstruction method with an *MSE* around $10^{-24}$ (virtually 0). 

The non weighted spectrum and the normalizing matrix (based on the magnitude of the sub-bands filters) is shown in the future figure.

![filter_mag](filter_mag.png)

## Generations 

Using this fusion method i tried to generate again few images using few clusters (64), 16 layers and only 300 images for training. 

s already introduced before there is two possible way to choose the following centroid to use for the generation, one is totally random and another it takes in account the correlation between the current layer and next one giving more realistic results but less variates as shown in the following figure.

![small_cl_gen_image_yaleb](small_cl_gen_image_yaleb.png)

It is important to see that at difference of what most of most of *GANs* do I generate images of the same size of the training set.

With random features selection the results can present more artifacts  and look some how less realistic.

![rand_gen_image_yaleb](rand_gen_image_yaleb.png)

However if downscaled as is done in most of the cases with *GANs* this look much better and realistic.

![rand_gen_image_yaleb_lr](rand_gen_image_yaleb_lr.png)

### MNIST

My next goal is to try what I can do with the very simple and already clustered *MNIST* dataset containing handwritten digits. The goal is to generate realistic digits. The over-complete transform over a sample of this dataset looks like the following picture.

![odct_mnist](odct_mnist.png)

Again the reconstruction from this representation is perfect with an error $\le 10^{-19} $. 

The over-complete representation of a single digit of the dataset (around 6000 images) with 16 layers it takes around 5Gb of memory. The number of cluster chosen is $1/10^{th}$ of the total number of samples for the current digit. Of curse with such a huge amount of data it takes more time to create the clusters.

The generation of numbers four works quite well even if many of it doesn't look totally naturals:

![](four_gen.png)

Overall the line is ticker and blurrier than in the real four, to be sure that those four where not simply images for the training set I tried to look for the closest image on the training set to see what it would look alike:

![Closest entries](closest_four.png)

 As it possible to see the resemblance is very high in some case is even the same, performing a fully random generation will probably solve this problem while generating more artifacts.
