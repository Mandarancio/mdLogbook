---
date: 15.11.2017
title: 'Thresholding and Soft K-Means'
---

## Negative and Positive decomposition and Tresholding

To increase the clustering results and improve the generation and reconstruction it is possible to split the over-complete *DCT* transformation in the negative and positive parts.

![pos_neg_decomposition](pos_neg_decomposition.png)

From this is possible to reconstruct the two part of the images and adding the two components at the transform level or at end of the reconstruction is possible to fully recover the original image.

![pos_neg_reconstruction](pos_neg_reconstruction.png)

To improve the clustering is also possible to remove the small components using a threshold, however this threshold has to be computed differently for each layer, as the magnitude at each layer is decreasing, I used a value proportional to the mean magnitude of each layer.

![treshold_by_level](treshold_by_level.png)

From  the thresholded decomposition is possible to reconstruct a very close representation of the original image, of curse more the threshold is small more the reconstruction will be correct, in the figure below is possible to see the reconstruction using a high threshold for each layer equivalent to a quarter of the mean magnitude.

![treshold_reconstruction](treshold_reconstruction.png)



## Soft K-Means

An improvement over classic *K-Means* is to use a soft version that instead of having a binary weight for each cluster (meaning that a sample is represented by only a cluster centroid) it compute a weight proportional to the distance from the cluster. 

There are 2 main parameters $\Omega$ that describe a maximum distance that a centroid can be used to reconstruct a sample and $\beta$ a normalization factor the weight is computed as:
$$
w_{i,j}=\frac{e^{-||x_j-c_i||^2_2}}{\sum_{c \in \Omega}e^{-||x_j-c||^2_2}}
$$
The weight matrix for a simple simulation look something like this:

![cluster_weight](cluster_weight.png)

where the $y$ axis are the clusters while in the $x$ axis are the samples. The value of $\beta$ and $\Omega$ change the reconstruction of the set, and we can see how it impact it on simulation data in the image below.  

![error_beta_vs_omega](error_beta_vs_omega.png)

The weight of the cluster against the samples on the simulated data is shown in the plot below, with a small $\Omega$ in average only one cluster represent each sample.

![soft_kmeans.clusters](soft_kmeans.clusters.png)

The reconstruction of the samples is done using as follow:
$$
\hat{x_j} = \frac{\sum_{i=1}^{N}  c_i\cdot w_{i,j}}{\sum_{i=1}^Nw_{i,j}}
$$
