---
date: 16.11.2017
title: 'Random Filters and Pos-Neg Clustering'
---

## Random Filters

Instead of using pass-band filters is possible to use a bank of randomly generated filters to try to extract more variate and possibly more significant features. To do so I choose to generate 16 normal random matrices $8\times 8$ and "convolve" it whit the *MNIST* dataset (using the digit *3*). The result of such convolution is shown in the figure below.

![rand_conv](rand_conv.png)

The perfect reconstruction is clearly possible and even using the negative, positive division and the threshold to improve the clustering is actually possible to get better reconstructions as shown in the following figure.

![tresh_rec](tresh_rec.png)

The generation give as well good results with more variation over the sub-bands generation, but still with plenty of artifacts.

![generation](generation.png)

![generation](yaleb_generation.png)

## Pos-Neg Clustering

The positive and negative clustering work as predict, using a naive generation strategy give good results but as predicted many artifacts because there is no correlation between the negative and positive part of the signal.

![pos_neg_generation](pos_neg_generation.png)



