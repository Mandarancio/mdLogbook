---
date: 17.11.2017
title: 'Progress? maybe not...'
---

## Libraries Implementation

To clear up the notebook and to reuse the same code over the different datasets I choose to implement few support libraries:

* *ImageTools*: to load images and datasets 
* *ODCT*: for the over-complete *DCT* transform
* *ClustTools*: to create and manage the clusters

Moreover to increase the results readability I choose to make use of support *CSV* file that store the information about the cluster, the number of images, the dataset used and the setting used for the generation (n. of clusters, thresholds, negative and positive decomposition).

When creating a new cluster this *CSV* file it is automatically updated and thanks to that a unique name for the cluster file is generated as well. It is possible as well to access to the clusters by theirs IDs.

## Sampling and extrapolation

Then I tried to tackle down the sub-sampling problem, to do so I choose this approach:

* Find the $n$ more significant samples for the first sub-band (low frequencies) using the training dataset

* Sample the new signal using the support defined before and apply the $H_1$ filter on it

* Using the soft k-mean extrapolate the rest of the spectrum $\hat{X}_1$ for the first sub-band

* Store the reconstructed sub-band in the new over-complete representation as layer 1

* Extrapolate all the other layers from this one as: 
  $$
  \hat{X}_i= H_i\cdot\frac{\hat{X_1}}{H_1+\lambda}
  $$

* For each layer extrapolate the full spectrum using the soft k-means on given clusters

* Use each reconstructed layer as new layer of the over-complete representation

* Compute the inverse over-complete *DCT* transform

The value of $\Omega$ for the K-Means is clearly very important as define how many neighbors will be taken in account, instead $\beta$ has much less impact. To be studied more is the effect of some normalization of the data to better fuse the reconstructed spectrum.

I tried with both full and negative/positive clustering with small difference in results, however I never tried with thresholds.

Another think to try is to use all the previously reconstructed levels to estimate the next one, maybe with a weight compute with the *Manhattan distance* of the levels. Other test I want to perform is to choose as base layer a different layer instead of the first one (maybe in the center??) to see what is the best for reconstruction. I choose the first one as it contains most of the energy.

The tests were performed on the *YaleB* dataset using as clusters configuration the following settings:

| # images | # clusters | Positive/Negative | Threshold |
| -------: | ---------: | ----------------- | --------: |
|     1000 |        100 | false             |       0.0 |

The compression rate chosen is of around $3e^{-2}$, and a number of sample $n=1000$. The adaptive sub sample (a.k.a *Best-K*) give as result the image show below with an $mse=7.7e^{-4}$.

![bestk](bestk.png)

The simple $f_{avg}$ learned sub-sampling method give much worst results as shown below with an $mse=1.6e^{-3}$, more than the double of the adaptive method. 

![fadt](fadt.png)

Using the clusters and the soft *k-means* the reconstruction is around $60\%$ better than the $f_{avg}$ method but still around $40%$ worst than the adaptive, with an overall $mse=1.0e^-3$.

![reconstructed](reconstructed.png)





## Further work

The further work will be focused on the estimation of the different layers from the previous ones, a more scientific evaluation of the impact of $\Omega$ and $\beta$ over the reconstruction performance and as well a optimization of the sampling and cluster selection techniques.

It is also important to review the inverse over-complete transform method chosen.
